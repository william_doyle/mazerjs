const size = 5;
const maze = Array.from({ length: size }, () => Array.from({ length: size }, () => ({
    visited: false,
    walls: {
        north: true,
        south: true,
        east: true,
        west: true
    }
})));

const showCell = cell => {
    const {north, east, south, west} = cell.walls;
    const g = [
        ['⬛', '⬛', '⬛'],
        ['⬛', '⬜', '⬛'],
        ['⬛', '⬛', '⬛']
    ];
    const gstring = g.map(row => row.join('')).join('');
    return gstring;
};
const showMazeRow = row => row.reduce((acc, cell) => `${acc}${showCell(cell)}}`, ''); 
const show = _maze => _maze.map(showMazeRow).join('\n');

console.log(show(maze))

let current = maze[2][2];

current.visited = true;

