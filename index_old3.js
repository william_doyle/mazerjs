class Maze {
    constructor (width, height, start = null, end = null) {
        this.width = width;
        this.height = height;

        this.arr = Array.from({ length: height }, () => Array.from({ length: width }, () => 1));

        if (!start) {
            this.start = { x: 1, y: 1 };
        }
        if (!end) {
            this.end = { x: width - 2, y: height - 2 };
        }

        this.recursiveBacktrace();
    }

    is_wall (x, y) {
        if ((0 <= y) && (y < this.height ) && (0 <= x) && (x < this.width )) {
            return this.arr[y][x] === 1;
        }
        return false;
    }

    recursiveBacktrace() {
        const stack = [{ x: this.start.x, y: this.start.y }];
        while (stack.length > 0) {
            const {x, y} = stack.pop();
            const directions =  [[0, 1], [0, -1], [1, 0], [-1, 0]];


          
        }


    }

    getNeighbors(current) {
        const neighbors = [];
        if (current.x > 0) {
            neighbors.push({ x: current.x - 1, y: current.y });
        }
        if (current.x < this.width - 1) {
            neighbors.push({ x: current.x + 1, y: current.y });
        }
        if (current.y > 0) {
            neighbors.push({ x: current.x, y: current.y - 1 });
        }
        if (current.y < this.height - 1) {
            neighbors.push({ x: current.x, y: current.y + 1 });
        }
        return neighbors;
    }

    toString() {
        return this.arr.map(row => row.map(cell => cell === 0 ? '⬜' : '⬛').join('')).join('\n');
    }
}


const m = new Maze(11, 11);
console.log(m);
console.log(m.toString());