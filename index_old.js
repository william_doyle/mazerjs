
//const mazeSize = 42;
const mazeSize = 10;
const EMPTY = '  ';
// const EMPTY = '⬜';

class MazeCell {
    #state;
    #visited;

    setState(state) {
        const valid = [EMPTY, '⬛', '🚶', '🏁'];
        if (!valid.includes(state))
            throw new Error('Invalid state');
        this.#state = state;
        // this.#visited = false;
    }

    visit() {
        this.#visited = true;
        this.#state = '⬜';
    }

    get visited() {
        return this.#visited;
    }

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.setState(EMPTY);
        this.#visited = false;
    }

    getState() {
        return this.#state;
    }

    erase() {
        this.setState(EMPTY);
    }

    draw() {
        this.setState('⬛');
    }

    toString() {
        return this.#state;
    }
}

const maze = Array.from({ length: mazeSize }, (_, i) => Array.from({ length: mazeSize }, (_, k) => new MazeCell(k, i)));

function showMaze(maze) {
    for (let i = 0; i < maze.length; i++) {
        for (let j = 0; j < maze[i].length; j++)
            process.stdout.write(maze[i][j].toString());
        console.log();
    }
}

function addWallPart(maze, x, y) {
    maze[x][y].visit();
}

function addWall(maze, startX, startY, endX, endY) {
    for (let i = startX; i <= endX; i++) {
        addWallPart(maze, i, startY);
        addWallPart(maze, i, endY);
    }
    for (let i = startY; i <= endY; i++) {
        addWallPart(maze, startX, i);
        addWallPart(maze, endX, i);
    }
}

function eraseWallPart(maze, x, y) {
    maze[x][y].erase();
}

function addBoarderWalls(maze) {
    function addNorthWall(maze) {
        addWall(maze, 0, 0, 0, mazeSize - 1);
        eraseWallPart(maze, 0, 1); // add hole for start point
    }

    function addSouthWall(maze) {
        addWall(maze, mazeSize - 1, 0, mazeSize - 1, mazeSize - 1);
        eraseWallPart(maze, mazeSize - 1, mazeSize - 2); // add hole for start point
    }

    function addEastWall(maze) {
        addWall(maze, 0, 0, mazeSize - 1, 0);
    }

    function addWestWall(maze) {
        addWall(maze, 0, mazeSize - 1, mazeSize - 1, mazeSize - 1);
    }

    addNorthWall(maze);
    addSouthWall(maze);
    addEastWall(maze);
    addWestWall(maze);
}

function addStartPoint(maze) {
    maze[0][1].setState('🚶');
}

function addEndPoint(maze) {
    maze[mazeSize - 1][mazeSize - 2].setState('🏁');
}

function halfTooEven(maze) {
    const a = Math.floor(maze.length / 2);
    // if a is even
    if (a % 2 === 0)
        return a;

    const b = Math.floor(maze.length / 2) + 1;
    if (b < maze.length)
        return b;

    const c = Math.floor(maze.length / 2) - 1;
    if (c > 0)
        return c;
    throw new Error('maze is too small');
}

function randomOddUnder(max) {
    return Math.floor(Math.random() * max) * 2 + 1;
}

function halfTooOdd(maze) {
    const a = Math.floor(maze.length / 2);
    // if a is even
    if (a % 2 !== 0)
        return a;

    const b = Math.floor(maze.length / 2) + 1;
    if (b < maze.length)
        return b;

    const c = Math.floor(maze.length / 2) - 1;
    if (c > 0)
        return c;
    throw new Error('maze is too small');
}

function addVerticalWall(maze) {
    addWall(maze, 0, halfTooEven(maze), maze.length - 1, halfTooEven(maze));
    eraseWallPart(maze, halfTooOdd(maze), halfTooEven(maze)); // add a hole to an odd position on the wall we just added
}

function addHorizontalWall(maze) {
    addWall(maze, halfTooEven(maze), 0, halfTooEven(maze), maze[0].length - 1);
    eraseWallPart(maze, halfTooEven(maze), halfTooOdd(maze)); // add a hole to an odd position on the wall we just added
}

// function addCross(maze) {
//     const guard = 5;
//     if (maze.length < guard || maze[0].length < guard)
//         return;
//     addVerticalWall(maze);
//     addHorizontalWall(maze);
//     eraseWallPart(maze, halfTooOdd(maze) + 2, halfTooEven(maze)); // add third hole to 1 of the walls

//     const topLeft = maze.slice(0, halfTooEven(maze)).map(row => row.slice(0, halfTooEven(maze)));
//     addCross(topLeft);

//     const topRight = maze.slice(0, halfTooEven(maze)).map(row => row.slice(halfTooEven(maze)));
//     addCross(topRight);

//     const bottomLeft = maze.slice(halfTooEven(maze)).map(row => row.slice(0, halfTooEven(maze)));
//     addCross(bottomLeft);

//     const bottomRight = maze.slice(halfTooEven(maze)).map(row => row.slice(halfTooEven(maze)));
//     addCross(bottomRight);
// }

function halfTooEven_NUMBER(n) {
    const a = Math.floor(n / 2);
    // if a is even
    if (a % 2 === 0)
        return a;

    const b = Math.floor(n / 2) + 1;
    if (b < n)
        return b;

    const c = Math.floor(n / 2) - 1;
    if (c > 0)
        return c;
    throw new Error('maze is too small');
}

function halfTooOdd_NUMBER(n) {
    const a = Math.floor(n / 2);
    // if a is even
    if (a % 2 !== 0)
        return a;

    const b = Math.floor(n / 2) + 1;
    if (b < n)
        return b;

    const c = Math.floor(n / 2) - 1;
    if (c > 0)
        return c;
    throw new Error('maze is too small');
}

function divideMaze(maze) {
    function vertical() {
        const length = maze.length;
        const mid_x = halfTooEven_NUMBER(length);
        maze.forEach(row => row.map(cell => {
            if ((cell.x === mid_x) && (cell.y !== halfTooOdd_NUMBER(length - 2)))
                cell.draw();
        }));
        return mid_x;
    }

    function horizontal() {
        const length = maze[0].length;
        const mid_y = halfTooEven_NUMBER(length);
        maze.map(row => row.map(cell => {
            if ((cell.y === mid_y) && (cell.x !== halfTooOdd_NUMBER(length - 2)))
                cell.draw();
        }));
        return mid_y;
    }
    if (maze.length <= maze[0].length) { 
        const wall_position = vertical();
        const left = maze.map(row => row.slice(0, wall_position));
        const right = maze.map(row => row.slice(wall_position));
        divideMaze(left);
        //divideMaze(right);
    }
    return horizontal();
}

addBoarderWalls(maze);
addStartPoint(maze);
addEndPoint(maze);

// addCross(maze);
// divideMaze(maze)
/*
    Choose the initial cell, mark it as visited and push it to the stack
    While the stack is not empty
        Pop a cell from the stack and make it a current cell
        If the current cell has any neighbours which have not been visited
            Push the current cell to the stack
            Choose one of the unvisited neighbours
            Remove the wall between the current cell and the chosen cell
            Mark the chosen cell as visited and push it to the stack

*/
const stack = [];
//Choose the initial cell, mark it as visited and push it to the stack
let initial_cell = maze[5][5];
initial_cell.visit();
stack.push(initial_cell);

//While the stack is not empty
while (stack.length > 0) {
    // Pop a cell from the stack and make it a current cell
    const current = stack.pop();
    //  If the current cell has any neighbours which have not been visited
    const neighbours = (() => {
        const surroundings = [];
        const x = current.x;
        const y = current.y;
        if (x > 0)
            surroundings.push(maze[x - 1][y]);
        if (x < maze.length - 1)
            surroundings.push(maze[x + 1][y]);
        if (y > 0)
            surroundings.push(maze[x][y - 1]);
        if (y < maze[0].length - 1)
            surroundings.push(maze[x][y + 1]);
        return surroundings.filter(cell => !cell.visited );
    })();

    if (neighbours.length === 0)
        continue;

    // Push the current cell to the stack
    stack.push(current)

    // Choose one of the unvisited neighbours
    const chosen = neighbours[0];
    chosen.visit();
    // remove the wall between the current cell and the chosen cell
    const x = current.x;
    const y = current.y;
    if (chosen.x === x) {
        if (chosen.y > y)
            eraseWallPart(maze, x, y, x, chosen.y);
        else
            eraseWallPart(maze, x, chosen.y, x, y);
    }

    // chosen.draw();
    stack.push(chosen);

    
}

// const temp = maze[7][7];
// temp.draw()

showMaze(maze);

// check to make sure every cell in the maze is an instane of MazeCell
maze.forEach((row, i) => {
    row.forEach((cell, k) => {
        if (!(cell instanceof MazeCell)) {
            const errorMessage = `(i: ${i}, k: ${k}) not a MazeCell`;
            console.log(errorMessage);
            throw new Error(errorMessage);
        }
    });
});