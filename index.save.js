// initalize a 2d array of size n*n full of 1s
const n = 40;
const maze = Array.from({ length: n }, (_, i) => Array.from({ length: n }, (_, k) => ({
    color: 'white',
    i: i,
    k: k,
    isStart: false,
    isEnd: false,
})));

function showMaze() {
    console.log(' ' + maze[0].reduce((acc, _, i) => `${acc}${i % 10} `, ''));
    for (let i = 0; i < maze.length; i++) {
        process.stdout.write(`${i % 10}`);
        for (let k = 0; k < maze[i].length; k++) {
            const cell = maze[i][k];
            process.stdout.write(cell.color === 'white' ? '⬛' : '⬜');
        }
        console.log();
    }
    console.log();
}

showMaze()

// add a boarder of walls
for (let i = 0; i < maze.length; i++) {
    maze[i][0].color = 'black';
    maze[i][n - 1].color = 'black';
    maze[0][i].color = 'black';
    maze[n - 1][i].color = 'black';
}

// mark start
maze[0][1].isStart = true;
maze[0][1].color = 'white';

// mark end
maze[n - 1][n - 2].isEnd = true;
maze[n - 1][n - 2].color = 'white';

showMaze()

const isEven = n => n % 2 === 0;
const isOdd = n => n % 2 !== 0;

function wallAttractor(x, low_x, high_x) {
    if (x < low_x || x > high_x)
        throw new Error('x is out of bounds');

    if (isOdd(x))
        return x;
    if (x + 1 < high_x)
        return x + 1;
    if (x - 1 > low_x)
        return x - 1;
    return undefined; // indicate that we cannot add a wall
}



// function addWalls(_m) {
//     const guard = 4;
//     if (_m.length < guard || _m[0].length < guard)
//         return;

//     function verticalCase() {
//         // divide the maze in half
//         const wallposition = wallAttractor(Math.floor(_m[0].length / 2), 0, _m[0].length - 1)
//         if (wallposition === undefined)
//             return;
//         const halfMaze = _m.map(row => row.slice(0, wallposition));
//         let have_hole = false;
//         halfMaze.forEach((row, index) => {
//             if (isEven(index) && !have_hole) {
//                 have_hole = true;
//                 return;
//             }
//             row[row.length - 1].color = 'black';
//         });

//         showMaze()
//         // recursive call to add walls
//         addWalls(halfMaze);
//         const halfMaze2 = _m.map(row => row.slice(wallposition));
//         addWalls(halfMaze2);
//     }

//     function horizontalCase() {
//         // divide the maze in half
//         const wallposition = wallAttractor(Math.floor(_m.length / 2), 0, _m.length - 1)
//         if (wallposition === undefined)
//             return;
//         const halfMaze = _m.slice(0, wallposition);
//         let have_hole = false;
//         // halfMaze[halfMaze.length - 1].forEach(cell => cell.color = 'black');
//         halfMaze[halfMaze.length - 1].forEach((cell, index) => {
//             if (isEven(index) && !have_hole) {
//                 have_hole = true;
//                 return;
//             }
//             cell.color = 'black';
//         });

//         // add a hole to the new wall
//         halfMaze[Math.floor(halfMaze.length / 2)][Math.floor(halfMaze[0].length / 2)].color = 'white';

//         showMaze()
//         // recursive call to add walls
//         addWalls(halfMaze);
//         const halfMaze2 = _m.slice(wallposition);
//         addWalls(halfMaze2);
//     }

//     if (_m.length < _m[0].length)
//         return verticalCase();
//     if (_m.length > _m[0].length)
//         return horizontalCase();
//     if (_m.length === _m[0].length) {
//         // randomly choose a case
//         if (Math.random() < 0.5)
//             return verticalCase();
//         return horizontalCase();
//     }

// }

// addWalls(maze)


function addWalls(start, end) {
    const guard = 4;
    if (end.x - start.x < guard || end.y - start.y < guard)
        return;

    function verticalCase() {
        // divide the maze in half
        // const wallposition = wallAttractor(Math.floor(_m[0].length / 2), 0, _m[0].length - 1)
        const wallposition = wallAttractor(Math.floor( ((end.x - start.x)/ 2) + (end.x - start.x)), start.x, end.x)
        if (wallposition === undefined)
            return;
        const halfMaze = _m.map(row => row.slice(0, wallposition));
        let have_hole = false;
        halfMaze.forEach((row, index) => {
            if (isEven(index) && !have_hole) {
                have_hole = true;
                return;
            }
            row[row.length - 1].color = 'black';
        });

        showMaze()
        // recursive call to add walls
        addWalls(halfMaze);
        const halfMaze2 = _m.map(row => row.slice(wallposition));
        addWalls(halfMaze2);
    }

    function horizontalCase() {
        // divide the maze in half
        const wallposition = wallAttractor(Math.floor(_m.length / 2), 0, _m.length - 1)
        if (wallposition === undefined)
            return;
        const halfMaze = _m.slice(0, wallposition);
        let have_hole = false;
        // halfMaze[halfMaze.length - 1].forEach(cell => cell.color = 'black');
        halfMaze[halfMaze.length - 1].forEach((cell, index) => {
            if (isEven(index) && !have_hole) {
                have_hole = true;
                return;
            }
            cell.color = 'black';
        });

        // add a hole to the new wall
        halfMaze[Math.floor(halfMaze.length / 2)][Math.floor(halfMaze[0].length / 2)].color = 'white';

        showMaze()
        // recursive call to add walls
        addWalls(halfMaze);
        const halfMaze2 = _m.slice(wallposition);
        addWalls(halfMaze2);
    }

    if (_m.length < _m[0].length)
        return verticalCase();
    if (_m.length > _m[0].length)
        return horizontalCase();
    if (_m.length === _m[0].length) {
        // randomly choose a case
        if (Math.random() < 0.5)
            return verticalCase();
        return horizontalCase();
    }

}

showMaze({x:0, y:0}, {x: maze.length, y:maze[0].length} )

