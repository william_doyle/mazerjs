const stack = [];
const size = 5;
class Cell {
    constructor() {
        this.northWall = true;
        this.southWall = true;
        this.eastWall = true;
        this.westWall = true;
        this.visited = false;
    }

    setNorthNeighbor(cell) {
        this.northNeighbor = cell;
    }
    setSouthNeighbor(cell) {
        this.southNeighbor = cell;
    }
    setEastNeighbor(cell) {
        this.eastNeighbor = cell;
    }
    setWestNeighbor(cell) {
        this.westNeighbor = cell;
    }
    setVisited() {
        this.visited = true;
    }

    getDetails() {
        return [
            [true, this.northWall, true],
            [this.westWall, false, this.eastWall],
            [true, this.southWall, true],
        ]
    }

    // getWalls () {
    // return this.getDetails().map(row => row.map(cell => `⬛⬛⬛\n⬛⬛⬛\n⬛⬛⬛`))
    // }
}

const maze = Array.from({ length: size }, () => Array.from({ length: size }, () => new Cell()));

function setupGraph() {
    maze.forEach((row, i) => {
        row.forEach((cell, j) => {
            if (i === 0) {
                cell.northWall = false;
                cell.setNorthNeighbor(null);
            }
            else {
                cell.setNorthNeighbor(maze[i - 1][j]);
            }

            if (i === size - 1) {
                cell.southWall = false;
                cell.setSouthNeighbor(null);
            }
            else {
                cell.setSouthNeighbor(maze[i + 1][j]);
            }

            if (j === 0) {
                cell.westWall = false;
                cell.setWestNeighbor(null);
            }
            else {
                cell.setWestNeighbor(maze[i][j - 1]);
            }

            if (j === size - 1) {
                cell.eastWall = false;
                cell.setEastNeighbor(null);
            }
            else {
                cell.setEastNeighbor(maze[i][j + 1]);
            }
        });
    });
}

function showMaze() {
    for (let i = 0; i < maze.length; i++) {
        for (let k = 0; k < maze[i].length; k++) {
            const cell = maze[i][k];
            process.stdout.write(`${cell.northWall ? '⬛' : '⬜'}`);
        }
        for (let k = 0; k < maze[i].length; k++) {
            const cell = maze[i][k];
            process.stdout.write(`${cell.westWall ? '⬛' : '⬜'}`);
        }
        for (let k = 0; k < maze[i].length; k++) {
            const cell = maze[i][k];
            process.stdout.write(`${cell.southWall ? '⬛' : '⬜'}`);
        }
        console.log();
    }

}

showMaze();