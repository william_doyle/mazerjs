// initalize a 2d array of size n*n full of 1s
const n = 40;
const maze = Array.from({ length: n }, (_, i) => Array.from({ length: n }, (_, k) => ({
    color: 'white',
    i: i,
    k: k,
    isStart: false,
    isEnd: false,
    isHole: false,
    isError: false,
})));

const logs = ['begin logs'];

function showMaze() {
    console.log(' ' + maze[0].reduce((acc, _, i) => `${acc}${i % 10} `, ''));
    for (let i = 0; i < maze.length; i++) {
        process.stdout.write(`${i % 10}`);
        for (let k = 0; k < maze[i].length; k++) {
            const cell = maze[i][k];
            if (cell?.isHole === true) {
                process.stdout.write('H ');
                continue;
            }

            if (cell.isStart) {
                process.stdout.write('S ');
                continue;
            }

            if (cell.isEnd) {
                process.stdout.write('F ');
                continue;
            }

            if (cell.isError) {
                process.stdout.write('e ');
                continue;
            }

            process.stdout.write(cell.color === 'white' ? '⬛' : '⬜');
        }
        console.log();
    }
    console.log();
}

showMaze()

// add a boarder of walls
for (let i = 0; i < maze.length; i++) {
    maze[i][0].color = 'black';
    maze[i][n - 1].color = 'black';
    maze[0][i].color = 'black';
    maze[n - 1][i].color = 'black';
}

// mark start
maze[0][1].isStart = true;
maze[0][1].color = 'white';

// mark end
maze[n - 1][n - 2].isEnd = true;
maze[n - 1][n - 2].color = 'white';

showMaze()

const isEven = n => {
    if (n === 0)
        return false;
    return n % 2 === 0;
}
// const isOdd = n => n % 2 !== 0;
const isOdd = n => {
    if (n === 0)
        return false;
    return n % 2 !== 0;
}

function wallAttractor(x, low_x, high_x) {
    if (x < low_x || x > high_x)
        throw new Error('x is out of bounds');

    if (isOdd(x))
        return x;
    if (x + 1 < high_x)
        return x + 1;
    if (x - 1 > low_x)
        return x - 1;
    return undefined; // indicate that we cannot add a wall
}


function toTwoVerticalHalfs(chamber) {
    // const wallposition = wallAttractor(Math.floor(chamber[0].length / 2), 1, chamber[0].length - 1)
    // console.log(wallposition);
    // if (wallposition === undefined)
    //     return undefined;


    const low_i = chamber[0][0]['i'];                                                           // get i value of first cell in chamber 
    const high_i = chamber[chamber.length - 1][chamber[chamber.length - 1].length - 1]['i'];    // ...            last  ...

    const GLOBAL_wallposition = wallAttractor(Math.floor(high_i / 2), low_i, high_i);
    if (GLOBAL_wallposition === undefined)
        return undefined;
    const temp_row = chamber.findIndex(row => row[0].i === GLOBAL_wallposition);
    console.log(`high i: '${high_i}', low i: '${low_i}', GLOBAL_wallposition: ${GLOBAL_wallposition}, temp_row: ${temp_row}`);
    // const LOCAL_wallposition = GLOBAL_wallposition;
    const LOCAL_wallposition = temp_row;


    // return {
    //     a: chamber.map(row => row.slice(0, LOCAL_wallposition)),
    //     b: chamber.map(row => row.slice(LOCAL_wallposition)),
    // }  
    return {
        // a: chamber.map(row => row.slice(0, GLOBAL_wallposition)),
        a: chamber.map(row => row.filter(el => el.i > 0 && el.i <  GLOBAL_wallposition)),
        b: chamber.map(row => row.filter(el => el.i < 0 && el.i >  GLOBAL_wallposition)),
    }
}

function addWalls(_m) {
    const guard = 4;
    if (_m.length < guard || _m[0].length < guard)
        return;

    function verticalCase() {

        // divide the maze in half
        // const wallposition = wallAttractor(Math.floor(_m[0].length / 2), 1, _m[0].length - 1)
        // if (wallposition === undefined) 
        //     return;
        const { a, b } = toTwoVerticalHalfs(_m);
        if (a === undefined)
            return;

        // const halfMaze = _m.map(row => row.slice(0, wallposition)); // hypothisys: this is not smart enough... we need to keep track based on GLOBAL position, not the local state of the length of these arrays
        let have_hole = false;
        const wall = [];

        for (const row of a) {
            const cell = row[row.length - 1];

            // skip values we don't need to visit
            if (cell.i === 0)
                continue;
            if (cell.i >= a.length - 1)
                continue;

            if (isEven(cell.k) && (!have_hole) && (cell.k !== 0) && (cell.i !== 0)) {
                have_hole = true;
                continue;
            }
            cell.color = 'black';
            wall.push(cell);
        }
        if (have_hole === false) {
            console.log(`Error state: showing broken maze. \n\tcase: vertical,\n\twall length: ${wall.length}\n\tcell positions:\n${JSON.stringify(wall.map(el => ({ i: el.i, k: el.k })), null, 2)}`);
            wall.forEach(cell => cell.isError = true)
            showMaze();
            throw new Error(`logic check failed. We made a wall with no hole [vertical]`);
        }



        showMaze()
        // recursive call to add walls
        addWalls(a);
        addWalls(b);
    }

    function horizontalCase() {
        // divide the maze in half
        const wallposition = wallAttractor(Math.floor(_m.length / 2), 1, _m.length - 1)
        if (wallposition === undefined)
            return;
        const halfMaze = _m.slice(0, wallposition);
        let have_hole = false;
        // halfMaze[halfMaze.length - 1].forEach(cell => cell.color = 'black');
        halfMaze[halfMaze.length - 1].forEach((cell, index) => {
            if (isEven(cell.i) && !have_hole && cell.k !== 0 && cell.i !== 0) {
                have_hole = true;
                cell.isHole = true;
                return;
            }
            cell.color = 'black';
        });
        if (have_hole === false) {
            console.log(`Error state: showing broken maze. Case: horizontal`);
            showMaze();
            throw new Error(`logic check failed. We made a wall with no hole [horizontal]`);
        }

        // add a hole to the new wall
        halfMaze[Math.floor(halfMaze.length / 2)][Math.floor(halfMaze[0].length / 2)].color = 'white';

        showMaze()
        // recursive call to add walls
        addWalls(halfMaze);
        const halfMaze2 = _m.slice(wallposition);
        addWalls(halfMaze2);
    }

    return verticalCase();

    // if (_m.length < _m[0].length)
    //     return verticalCase();
    // if (_m.length > _m[0].length)
    //     return horizontalCase();
    // if (_m.length === _m[0].length) {
    //     // randomly choose a case
    //     if (Math.random() < 0.5)
    //         return verticalCase();
    //     return horizontalCase();
    // }

}

addWalls(maze)

console.log(logs);